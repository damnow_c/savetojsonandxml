﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace SaveToJSONandXML
{
    class OperationsJSON
    {
        static string jsonFile = Directory.GetCurrentDirectory();

        public static void SerializeUsingJavaScriptSerializer(List<Category> listOfCategories)
        {
            using (StreamWriter file = File.CreateText(jsonFile + "SavedToJSON.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, listOfCategories);
            }
            Console.WriteLine(jsonFile + "SavedToJSON.json");
        }

        public static List<Category> DeserializeUsingJavaScriptSerializer(string fileName)
        {
            string json = File.ReadAllText(jsonFile + fileName + ".json");
            JavaScriptSerializer js = new JavaScriptSerializer();
            var jsonObject = js.Deserialize<List<Category>>(json);
            return jsonObject;

            Console.WriteLine(jsonFile + "SavedToJSON.json");
        }
    }
}
