﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveToJSONandXML
{
    public class Comunications
    {
        public static string AskForString(string whatData)
        {
            Console.Write($"{whatData}: ");
            return (Console.ReadLine());
        }

        public static int AskForIntFromTo(string whatData, int min, int max)
        {
            string data = "";
            int number = 0;
            Boolean success = false;
            do
            {
                Console.Write($"{whatData}: ");
                data = (Console.ReadLine());
                try
                {
                    number = Convert.ToInt32(data);
                    if (number >= min && number <= max)
                    {
                        success = true;
                    }
                }
                catch
                {
                    success = false;
                }
            } while (!success);
            return number;
        }

        public static List<Exercise> AddQuestion(List<Exercise> listOfQuestions)
        {
            Console.Clear();
            Console.WriteLine($"\nDodawanie nowego pytania:\n");
            Exercise newExercise = new Exercise();
            newExercise.Question = AskForString("Podaj treść pytania");
            newExercise.GoodAnswer = AskForString("Podaj poprawną odpowiedź");
            newExercise.BadAnswer1 = AskForString($"Podaj błędną odpowiedź (1 z 3)");
            newExercise.BadAnswer2 = AskForString($"Podaj błędną odpowiedź (2 z 3)");
            newExercise.BadAnswer3 = AskForString($"Podaj błędną odpowiedź (3 z 3)");
            listOfQuestions.Add(newExercise);
            Console.ReadKey();
            return listOfQuestions;
        }
        
        public static Category AddCategory()
        {
            Console.Clear();
            Console.WriteLine("Dodawanie nowej kategorii.\n");
            Category newCategory = new Category();
            newCategory.CategoryName = AskForString("Podaj nazwę kategorii");
            Console.ReadKey();
            return newCategory;
        }

        public static int menuBar ()
        {
            Console.Clear();
            int decyssion = 0;
            Console.WriteLine($"MENU\n1. Dodaj nową kategorię\n2. Dodaj pytanie do wybranej kategorii\n3. Przeprowadź test z wybranej kategorii\n4. Zapisz do pliku XML/JSON\n5. Wczytaj z pliku XML/JSON \n6. Zakończ!");
            decyssion = AskForIntFromTo("\nWybierz opcję",1,6);
            return decyssion;
        }
    }
}
