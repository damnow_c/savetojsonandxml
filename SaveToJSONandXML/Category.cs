﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveToJSONandXML
{
    public class Category
    {
        public string CategoryName { get; set; }
        public List<Exercise> Exercises { get; set; }

        public Category()
        {
            Exercises = new List<Exercise>();
        }

        public static int CategoryChoose(List<Category> listOfCategories)
        {
            Console.Clear();
            int index = 0;
            Console.WriteLine("Wybierz jedną z poniższych kategorii: \n");
            foreach (var category in listOfCategories)
            {
                index++;
                Console.WriteLine($"{index}. {category.CategoryName}");
            }
            return Comunications.AskForIntFromTo("\nWybrana kategoria", 1, listOfCategories.Count) - 1;
        }
    }
}
