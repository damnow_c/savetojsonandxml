﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;

namespace SaveToJSONandXML
{
    public class OperationsXML
    {
        static string xmlFile = Directory.GetCurrentDirectory();

        public static void SerializeToXmlFile(List<Category> listOfCategories)
        {
            var serializedObject = new XmlSerializer(typeof(List<Category>));
            {
                using (StreamWriter file = new StreamWriter(xmlFile + "SavedToXML.xml"))
                {
                    serializedObject.Serialize(file, listOfCategories);
                    file.Close();
                }
            }
        }
        
        public static List<Category> DeserializeObjectFromXml(string fileName)
        {
            List<Category> categoryList = new List<Category>();

            XmlSerializer xs = new XmlSerializer(typeof(Category[]));
            using (Stream ins = File.Open(xmlFile + fileName + ".xml", FileMode.Open))
                foreach (Category singleCategory in (Category[])xs.Deserialize(ins))
                    categoryList.Add(singleCategory);

            return categoryList;
        }   
    }
}
