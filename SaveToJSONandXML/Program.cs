﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;

namespace SaveToJSONandXML
{
    class Program :Comunications
    {
        static void Main(string[] args)
        {
            List<Category> categoryList = new List<Category>();
            Console.Clear();
            int currentCategory = 0;
            
            do
            {
                switch (menuBar())
                {
                    case 1:
                        categoryList.Add(AddCategory());
                        break;
                    case 2:
                        Console.Clear();
                        if (categoryList.Count == 0)
                        {
                            Console.WriteLine("Najpierw dodaj kategorię!\n\n");
                            categoryList.Add(AddCategory());
                        }
                        List<Exercise> exercisesTemporary = new List<Exercise>();
                        currentCategory = Category.CategoryChoose(categoryList);
                        exercisesTemporary = categoryList[currentCategory].Exercises;
                        categoryList[currentCategory].Exercises = AddQuestion(exercisesTemporary);
                        break;
                    case 3:
                        Console.Clear();
                        if (categoryList.Count == 0)
                        {
                            Console.WriteLine("\n\nBy przeprowadzić test najpierw musisz dodać jakąkolwiek kategorię!");
                            Console.ReadKey();
                        }
                        else
                        {
                            currentCategory = Category.CategoryChoose(categoryList);
                            if (categoryList[currentCategory].Exercises.Count == 0)
                            {
                                Console.WriteLine("Kategoria nie zawiera pytań!");
                                Console.ReadKey();
                            }
                            else
                            {
                                Test.MakeTestFromCategory(categoryList[currentCategory]);
                            }
                        }
                        break;
                    case 4:
                        Console.Clear();
                        Console.WriteLine("Podaj wybrany format zapisu:\n\n1. XML\n2. JSON\n3. Powrót do menu");
                        switch (AskForIntFromTo("\nWybrana opcja", 1, 3))
                        {
                            case 1:
                                OperationsXML.SerializeToXmlFile(categoryList);
                                Console.WriteLine("\nZAKOŃCZONO ZAPIS SavedToXML.xml ! Naciśnij dowolni klawisz, by przejśc do MENU");
                                Console.ReadKey();
                                break;
                            case 2:
                                OperationsJSON.SerializeUsingJavaScriptSerializer(categoryList);
                                Console.WriteLine("\nZAKOŃCZONO ZAPIS SavedToJASON.json ! Naciśnij dowolni klawisz, by przejśc do MENU");
                                Console.ReadKey();
                                break;
                            case 3:
                                break;
                            default:
                                break;
                        }
                        break;
                    case 5:
                        Console.Clear();
                        Console.WriteLine("Podaj wybrany format odczytu:\n\n1. XML\n2. JSON\n3. Powrót do menu");
                        //formatOption = AskForIntFromTo("Wybrana opcja", 1, 3);
                        switch (AskForIntFromTo("\nWybrana opcja", 1, 3))
                        {
                            case 1:
                                string xmlFile = AskForString("\nPodaj nazwę pliku z katalogu Files bez rozszrzenia (np. SavedToXML)");
                                if (File.Exists(Directory.GetCurrentDirectory() + xmlFile + ".xml"))
                                {
                                    categoryList = OperationsXML.DeserializeObjectFromXml(xmlFile);
                                    Console.WriteLine("\nPlik został wczytany. Wciśnij dowolny klawisz, by rpzejść do MENU!");
                                }
                                else
                                {
                                    Console.WriteLine("\nPodany plik nie istnieje. Wciśnij dowolny klawisz, by przejść do MENU!");
                                }
                                Console.ReadKey();
                                break;
                            case 2:
                                string jsonFile = AskForString("\nPodaj nazwę pliku z katalogu Files bez rozszrzenia (np. SavedToJSON)");
                                if (File.Exists(Directory.GetCurrentDirectory() + jsonFile + ".json"))
                                {
                                    categoryList = OperationsJSON.DeserializeUsingJavaScriptSerializer(jsonFile);
                                    Console.WriteLine("\nPlik został wczytany. Wciśnij dowolny klawisz, by rpzejść do MENU!");
                                }
                                else
                                {
                                    Console.WriteLine("\nPodany plik nie istnieje. Wciśnij dowolny klawisz, by przejść do MENU!");
                                }
                                Console.ReadKey();
                                break;
                            case 3:
                                break;
                            default:
                                break;
                        }
                        break;
                    case 6:
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            } while (true);

        }
        
    }
}
