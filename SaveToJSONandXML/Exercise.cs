﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveToJSONandXML 
{
    public class Exercise
    {
        public string Question { get; set; }
        public string GoodAnswer { get; set; }
        public string BadAnswer1 {get; set; }
        public string BadAnswer2 { get; set; }
        public string BadAnswer3 { get; set; }

        public static Boolean AskQuestion(Exercise exercise)
        {
            Console.Clear();
            Console.WriteLine($"Pytanie:  {exercise.Question}");
            Console.WriteLine("\nWybierz odpowiedź:");
            int userAnswer = 0;
            List<int> orderList = new List<int>(4);
            int newOrder;
            Boolean repeatSearchingOrder = false;
            Random rnd = new Random();

            for (int i = 0; i < 4; i++)
            {
                do
                {
                    newOrder = rnd.Next(0, 4);
                    repeatSearchingOrder = false;
                    foreach (var usedOrder in orderList)
                    {
                        if (usedOrder == newOrder)
                        {
                            repeatSearchingOrder = true;
                        }
                    }
                } while (repeatSearchingOrder == true);
                orderList.Add(newOrder);

                Console.Write($"{i + 1}. ");
                switch (newOrder)
                {
                    case 0:
                        Console.WriteLine($"{exercise.GoodAnswer}");
                        break;
                    case 1:
                        Console.WriteLine($"{exercise.BadAnswer1}");
                        break;
                    case 2:
                        Console.WriteLine($"{exercise.BadAnswer2}");
                        break;
                    case 3:
                        Console.WriteLine($"{exercise.BadAnswer3}");
                        break;
                    default:
                        break;
                }
            }

            userAnswer = Comunications.AskForIntFromTo("\nPodaj twój typ odpowiedzi", 1, 4);
            if (orderList[userAnswer - 1] == 0)
            {
                Console.WriteLine("\nGratulacje! Odpowiedź prawidłowa.");
                Console.ReadKey();
                return true;
            }
            else
            {
                Console.WriteLine("\nNiestety... Pudło.");
                Console.ReadKey();
                return false;
            }
        }
    }
}
