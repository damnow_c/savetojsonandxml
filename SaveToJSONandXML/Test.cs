﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveToJSONandXML
{
    class Test : Comunications
    {
        public static int GoodAnswers { get; set; }
        public static int AllQuestions { get; set; }

        public static void MakeTestFromCategory (Category fromCategory)
        {
            AllQuestions = fromCategory.Exercises.Count;
            List<int> usedOrderList = new List<int>();
            int newOrder;
            Boolean repeatSearchingOrder = false;
            Random rnd = new Random();
            GoodAnswers = 0;

            foreach (var exercise in fromCategory.Exercises)
            {
                Console.Clear();
                do
                {
                    newOrder = rnd.Next(0, AllQuestions);
                    repeatSearchingOrder = false;
                    foreach (var usedOrder in usedOrderList)
                    {
                        if (usedOrder == newOrder)
                        {
                            repeatSearchingOrder = true;
                        }
                    }
                } while (repeatSearchingOrder == true);
                usedOrderList.Add(newOrder);
                if (Exercise.AskQuestion(fromCategory.Exercises[newOrder]) == true )
                {
                    GoodAnswers++;
                }
                Console.WriteLine($"\n\nUzyskany wynik: {GoodAnswers}/{AllQuestions} ({(System.Convert.ToDouble(GoodAnswers) / System.Convert.ToDouble(AllQuestions) * 100):F2}%)");

            }
            Console.WriteLine("\n\nWciśnij dowolny przycisk, by powrócić do MENU.");
            Console.ReadKey();
        }
    }
}
